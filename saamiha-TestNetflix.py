#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, predict
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_movieid_parsing(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue().split("\n")[0], "10040:")
    
    def test_output_length(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            len(w.getvalue().split("\n"))-1, 5)    
            
    def test_RMSE(self):
        with open("RunNetflix.in",'r') as f:
            data = f.read()
        r = StringIO(data)
        w = StringIO()
        netflix_eval(r, w)
        self.assertLess(
            float(w.getvalue().split("\n")[1883]), 0.95)
    
    def test_prediction(self):
        movie = "1000"
        cust = "2326571"
        pred = predict(cust,movie)
        self.assertEqual(
            pred, 3.3)       
 
    def test_prediction_lessthan1p0(self):
        movie = "10054"
        cust = "1287459"
        pred = predict(cust,movie)
        self.assertEqual(
                pred, 1.0)   
            
    def test_prediction_greaterthan5p0(self):
        movie = "10002"
        cust = "308502"
        pred = predict(cust,movie)
        self.assertEqual(
            pred, 5.0)       
# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
